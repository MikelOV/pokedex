package com.trade.pokedex.service.impl;

import com.trade.pokedex.domain.Pokemon;
import com.trade.pokedex.domain.Type;
import com.trade.pokedex.dto.PokemonDTO;
import com.trade.pokedex.mapper.PokemonMapper;
import com.trade.pokedex.repository.PokemonRepository;
import com.trade.pokedex.service.PokemonService;
import com.trade.pokedex.web.exception.PokemonNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PokemonServiceImpl implements PokemonService {


    private final PokemonRepository repository;

    private final PokemonMapper mapper;

    public Flux<PokemonDTO> getByNameLike(String name) {
        return repository.findByNameLike(name).map(mapper::toDTO);
    }

    public Flux<PokemonDTO> getAll() {
        return repository.findAll().map(mapper::toDTO);
    }

    public Mono<Void> invertFavorite(int id) {
        return getById(id)
                .flatMap(pokemon -> {
                    pokemon.setFavorite(!pokemon.isFavorite());
                    return repository.save(pokemon).then();
                });
    }

    public Mono<PokemonDTO> getPokemonDetail(int id) {
        return getById(id).map(mapper::toDTO);
    }

    public Flux<PokemonDTO> getAllFavorites() {
        return repository.findByFavorite(true).map(mapper::toDTO);
    }

    public Flux<PokemonDTO> getByType(List<Type> types) {
        return repository.findByTypeIn(types).map(mapper::toDTO);
    }

    private Mono<Pokemon> getById(int id) {
        return repository.findById(id).switchIfEmpty(Mono.error(new PokemonNotFoundException(id)));
    }


}
