package com.trade.pokedex.service;

import com.trade.pokedex.domain.Type;
import com.trade.pokedex.dto.PokemonDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface PokemonService {

    Flux<PokemonDTO> getByNameLike(String name);

    Flux<PokemonDTO> getAll();

    Mono<Void> invertFavorite(int id);

    Mono<PokemonDTO> getPokemonDetail(int id);

    Flux<PokemonDTO> getAllFavorites();

    Flux<PokemonDTO> getByType(List<Type> types);

}
