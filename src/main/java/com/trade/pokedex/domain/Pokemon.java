package com.trade.pokedex.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Document
@Data
public class Pokemon {

    @Id
    private Integer id;

    private String name;

    private String num;

    private List<Type> type;

    private String img;

    private String candy;

    private String egg;

    @Field("spawn_chance")
    private double spawnChange;

    private List<Type> weaknesses;

    private String height;

    private String weight;

    private boolean favorite;
}
