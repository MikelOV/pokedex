package com.trade.pokedex.domain;

public enum Type {

    Grass,
    Poisson,
    Fire,
    Ice,
    Flying,
    Psychic,
    Ground,
    Water,
    Electric,
    Poison,
    Bug,
    Normal,
    Fighting,
    Rock,
    Ghost,
    Dark,
    Dragon,
    Steel,
    Fairy
}
