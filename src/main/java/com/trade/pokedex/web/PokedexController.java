package com.trade.pokedex.web;

import com.fasterxml.jackson.annotation.JsonView;
import com.trade.pokedex.domain.Type;
import com.trade.pokedex.dto.PokemonDTO;
import com.trade.pokedex.service.PokemonService;
import com.trade.pokedex.web.views.PokemonViews;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/pokedex")
@RequiredArgsConstructor
@Tag(name = "Pokedex Controller", description = "Controller to retrieve pokemon information")
public class PokedexController {

    private final PokemonService service;

    @GetMapping("/name")
    @JsonView(PokemonViews.Public.class)
    @Operation(summary = "Get pokemons with name like", description = "Get all pokemons that name contains given string")
    public Flux<PokemonDTO> nameLike(@RequestParam String name) {
        return service.getByNameLike(name);
    }

    @GetMapping
    @Operation(summary = "Get all pokemons", description = "Get all the pokemons without any type of filter")
    @JsonView(PokemonViews.Public.class)
    public Flux<PokemonDTO> all() {
        return service.getAll();
    }

    @PatchMapping("/{pokemonId}")
    @Operation(summary = "Set or unset favorite tag", description = "Invert pokemon favorite tag by pokemon id")
    @ApiResponse(responseCode = "404", description = "Pokemon with given id not found",
            content = @Content(examples = @ExampleObject(value = "{\"message\" : \"Pokemon 878 is not found in the pokedex, you should contact professor Oak\"}")))
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Mono<Void> invertFavorite(@PathVariable @Parameter(example = "1", description = "Pokemon id") int pokemonId) {
        return service.invertFavorite(pokemonId);
    }

    @GetMapping("/{pokemonId}")
    @Operation(summary = "Get pokemon detail", description = "Get pokemon detail by given id")
    @ApiResponse(responseCode = "404", description = "Pokemon with given id not found",
            content = @Content(examples = @ExampleObject(value = "{\"message\" : \"Pokemon 878 is not found in the pokedex, you should contact professor Oak\"}")))
    @ResponseStatus(HttpStatus.OK)
    @JsonView(PokemonViews.Detail.class)
    public Mono<PokemonDTO> pokemonDetail(@PathVariable @Parameter(example = "1", description = "Pokemon id") int pokemonId) {
        return service.getPokemonDetail(pokemonId);
    }

    //TODO: name, type and favorite filter should be front task, pokemons are finite
    @GetMapping("/favorites")
    @Operation(summary = "Get favorites pokemons", description = "Get all pokemons that has favorite tag true")
    @JsonView(PokemonViews.Public.class)
    public Flux<PokemonDTO> name() {
        return service.getAllFavorites();
    }

    @GetMapping("/type")
    @Operation(summary = "Get pokemons by type", description = "Get all pokemons with given types")
    @JsonView(PokemonViews.Public.class)
    public Flux<PokemonDTO> type(@RequestParam @Parameter(description = "Filter types", example = "Poison,Grass") List<Type> types) {
        return service.getByType(types);
    }
}
