package com.trade.pokedex.web.exception;

public class PokemonNotFoundException extends RuntimeException{

    public PokemonNotFoundException(int id) {
        super("Pokemon:" + id +" is not found in the pokedex, you should contact professor Oak.");
    }
}
