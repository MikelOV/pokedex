package com.trade.pokedex.dto;

import com.fasterxml.jackson.annotation.JsonView;
import com.trade.pokedex.domain.Type;
import com.trade.pokedex.web.views.PokemonViews;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Data
public class PokemonDTO {

    @JsonView(PokemonViews.Public.class)
    @Schema(example = "1")
    private Integer id;

    @JsonView(PokemonViews.Public.class)
    @Schema(example = "Bulbasour")
    private String name;

    @JsonView(PokemonViews.Public.class)
    @Schema(example = "001")
    private String num;

    @JsonView(PokemonViews.Public.class)
    private List<Type> type;

    @JsonView(PokemonViews.Public.class)
    @Schema(example = "http://www.serebii.net/pokemongo/pokemon/001.png")
    private String img;

    @JsonView(PokemonViews.Detail.class)
    @Schema(example = "Bulbasaur Candy")
    private String candy;

    @JsonView(PokemonViews.Detail.class)
    @Schema(example = "2 km")
    private String egg;

    @JsonView(PokemonViews.Detail.class)
    @Schema(example = "0.89")
    private double spawnChange;

    @JsonView(PokemonViews.Detail.class)
    private List<Type> weaknesses;

    @JsonView(PokemonViews.Detail.class)
    @Schema(example = "0.71 m")
    private String height;

    @JsonView(PokemonViews.Detail.class)
    @Schema(example = "6.9 kg")
    private String weight;

    @JsonView(PokemonViews.Public.class)
    private boolean favorite;


}
