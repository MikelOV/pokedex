package com.trade.pokedex.repository;

import com.trade.pokedex.domain.Pokemon;
import com.trade.pokedex.domain.Type;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.List;

@Repository
public interface PokemonRepository extends ReactiveMongoRepository<Pokemon, Integer> {

    Flux<Pokemon> findByNameLike(String name);

    Flux<Pokemon> findByTypeIn(List<Type> type);

    Flux<Pokemon> findByFavorite(boolean isFavorite);
}
