package com.trade.pokedex.mapper;

import com.trade.pokedex.domain.Pokemon;
import com.trade.pokedex.dto.PokemonDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PokemonMapper {

    PokemonDTO toDTO(Pokemon entity);
}
