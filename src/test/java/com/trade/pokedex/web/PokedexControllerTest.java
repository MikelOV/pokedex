package com.trade.pokedex.web;

import com.trade.pokedex.dto.PokemonDTO;
import com.trade.pokedex.service.PokemonService;
import com.trade.pokedex.web.exception.PokemonNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = PokedexController.class)
public class PokedexControllerTest {

    @Autowired
    private WebTestClient webClient;

    @MockBean
    PokemonService pokemonService;

    //TODO: Just some unit testing but not every method and flow is cover, just for demo

    @Test
    void givenNameWhenGetByNameResponseOk() {
        String pokemonName = "Pikachu";

        when(pokemonService.getByNameLike(anyString())).thenReturn(Flux.just(buildMockPokemon()));

        webClient.get()
                .uri(uriBuilder ->
                        uriBuilder
                                .path("/pokedex/name")
                                .queryParam("name", pokemonName)
                                .build())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .jsonPath("$[0].favorite").isEqualTo(true)
                .jsonPath("$[0].candy").doesNotExist()
                .jsonPath("$[0].name").isEqualTo("Pikachu");

        Mockito.verify(pokemonService, times(1)).getByNameLike(pokemonName);
    }

    @Test
    void givenInvalidIdWhenGetDetailsNotFound() {
        int pokemonId = 5;
        when(pokemonService.getPokemonDetail(anyInt())).thenReturn(Mono.error(new PokemonNotFoundException(pokemonId)));

        webClient.get()
                .uri("/pokedex/{id}", pokemonId)
                .exchange()
                .expectStatus()
                .isNotFound()
                .expectBody()
                .jsonPath("$.message").isEqualTo("Pokemon:5 is not found in the pokedex, you should contact professor Oak.");

        Mockito.verify(pokemonService, times(1)).getPokemonDetail(pokemonId);
    }

    private PokemonDTO buildMockPokemon() {
        PokemonDTO responseMock = new PokemonDTO();
        //Detail
        responseMock.setCandy("my candy");
        //True
        responseMock.setFavorite(true);
        responseMock.setName("Pikachu");
        return responseMock;
    }
}
