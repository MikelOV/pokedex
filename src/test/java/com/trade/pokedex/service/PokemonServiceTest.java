package com.trade.pokedex.service;

import com.mongodb.assertions.Assertions;
import com.trade.pokedex.domain.Pokemon;
import com.trade.pokedex.mapper.PokemonMapper;
import com.trade.pokedex.mapper.PokemonMapperImpl;
import com.trade.pokedex.repository.PokemonRepository;
import com.trade.pokedex.service.impl.PokemonServiceImpl;
import com.trade.pokedex.web.exception.PokemonNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {PokemonServiceImpl.class, PokemonMapperImpl.class})
public class PokemonServiceTest {

    @Autowired
    PokemonService service;

    @MockBean
    PokemonRepository repository;

    @Autowired
    PokemonMapper mapper;

    @Captor
    ArgumentCaptor<Pokemon> pokemonCaptor;

    //TODO: Just some unit testing but not every method and flow is cover, just for demo

    @Test
    void invertFavoriteOk() {

        int id = 1;

        Pokemon pokemon = new Pokemon();
        pokemon.setFavorite(true);
        pokemon.setId(id);

        when(repository.findById(id)).thenReturn(Mono.just(pokemon));
        when(repository.save(any(Pokemon.class))).thenReturn(Mono.just(pokemon));

        StepVerifier.create(service.invertFavorite(id)).verifyComplete();

        Mockito.verify(repository, times(1)).findById(id);
        Mockito.verify(repository, times(1)).save(pokemonCaptor.capture());
        Assertions.assertFalse(pokemonCaptor.getValue().isFavorite());
    }

    @Test
    void getDetailNotFound() {

        int id = 1;
        when(repository.findById(id)).thenReturn(Mono.empty());

        StepVerifier.create(service.invertFavorite(id))
                .expectErrorMatches(throwable -> throwable instanceof PokemonNotFoundException);
    }


    @Test
    void getAllPokemonsOk() {

        Pokemon pokemon = new Pokemon();
        pokemon.setId(1);

        Pokemon pokemon2 = new Pokemon();
        pokemon2.setId(2);

        when(repository.findAll()).thenReturn(Flux.just(pokemon, pokemon2));

        StepVerifier.create(service.getAll())
                .expectNextMatches(p -> p.getId() == 1)
                .expectNextMatches(p -> p.getId() == 2)
                .verifyComplete();
    }

}
