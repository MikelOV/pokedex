# Pokedex

Api to retrieve information about pokemons.

The code has a Docker MongoDb image that is seed with the first generation of pokemons information.

## Getting started

1. Compile the project
    ```bash
    mvn clean install
    ```
2. Build the images and run the containers

    ```bash
    docker-compose up --build
    ```
   First time will download MongoDb image and first time creating the Mongo Container will fill the database with pokemon info stored in a json file

3. Once the containers are ready , the API is up to be consumed. You can see the Swagger documentation of the API in:
http://localhost:8080/swagger-ui.html

## Environment

Environment variables are set in an .env file.

For running the app without Docker, you can fill the environment variable of the mongo url in the application.properties file allocated in the resources package.




